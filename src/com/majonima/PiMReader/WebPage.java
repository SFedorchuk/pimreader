package com.majonima.PiMReader;

import android.app.Activity;
import android.net.Uri;
import android.os.Bundle;
import android.webkit.WebView;

import static com.majonima.PiMReader.MainActivity.getPage;

/**
 * Created by Станислав on 07.04.2015.
 */
public class WebPage extends Activity {
    static org.jsoup.nodes.Document doc = null;
    static WebView Content;
    static String page;
    static Uri uri;
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.article);
        //WebArticle();

        Content = (WebView) findViewById(R.id.ArticlePage);
        //Content.loadData(urls.toString(),"text/html","ru_Ru");
        Content.getSettings().setJavaScriptEnabled(true);
        Content.loadUrl(getPage());
        Content.getSettings().setBuiltInZoomControls(true);
    }
}
