package com.majonima.PiMReader;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;

import java.util.Iterator;
import java.util.LinkedHashSet;

import static com.majonima.PiMReader.MainActivity.*;

/**
 * Created by Станислав on 14.04.2015.
 */
public class TextActivity extends Activity implements View.OnClickListener {
    static int wrapContent = LinearLayout.LayoutParams.WRAP_CONTENT;
    static int matchParent = LinearLayout.LayoutParams.MATCH_PARENT;
    static LinkedHashSet<String> article=new java.util.LinkedHashSet<String>();
    static Object[] link;
    static int id;
    /**
     * Called when the activity is first created.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.text);
        LinearLayout llMain = (LinearLayout) findViewById(R.id.scrollViewText);
        LinearLayout.LayoutParams lParams = new LinearLayout.LayoutParams(
                matchParent, wrapContent);
        lParams.bottomMargin = 2;
        article=getArticle();
        link= getLink().toArray();
        Iterator<String> itr = article.iterator();
        int i=0;
        while (itr.hasNext()) {
            Button btnNew = new Button(this);
            btnNew.setText(itr.next().toString());
            btnNew.setId(i++);
            btnNew.setTextColor(Color.parseColor("#FFFFFF"));
            btnNew.setGravity(3);
            btnNew.setBackgroundColor(Color.parseColor("#000000"));
            btnNew.setOnClickListener(this);
            llMain.addView(btnNew, lParams);
        }
    }


    @Override
    public void onClick(View v) {
        id=v.getId();
        Log.d("link",link[id].toString());
        setPage(link[id].toString());
        Intent intent = new Intent(TextActivity.this, WebPage.class);
        startActivity(intent);
    }
}
