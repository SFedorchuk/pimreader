package com.majonima.PiMReader;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import static com.majonima.PiMReader.MainActivity.*;

/**
 * Created by Станислав on 20.04.2015.
 */
public class PagerViewActivity extends Activity{
    static LinearLayout swipe;

    Object[] pictureAuthor;
    Object[] linkAuthor;


    static List<View> pages;

    static LayoutInflater inflater;
    static ViewPager viewPager;
    static Context ctx;
    static int wrapContent = LinearLayout.LayoutParams.WRAP_CONTENT;
    static int matchParent = LinearLayout.LayoutParams.MATCH_PARENT;


    public static Context getCtx() {
        return ctx;
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.graficavp);
        ctx = this;
        linkAuthor = getLinkAutor().toArray();
        pictureAuthor =getPictureAutor().toArray();

        inflater = LayoutInflater.from(this);
        pages = new ArrayList<>();


        for(int i=0;i< pictureAuthor.length-1; i++){
            ImageView ImgView =new ImageView(ctx);

            pages.add(ImgView);
        }

        PageViewAdapter pagerAdapter = new PageViewAdapter(pages);
        viewPager = (ViewPager) findViewById(R.id.awesomepager) ;
        viewPager.setAdapter(pagerAdapter);




    }
    class PageViewAdapter extends PagerAdapter{
        List<View> pages = null;
        Context ctx=getCtx();

        public PageViewAdapter(List<View> pages){
            this.pages = pages;
        }

        @Override
        public Object instantiateItem(ViewGroup collection, int position){

            /*LinearLayout viewLayout = new LinearLayout(ctx);
            LinearLayout.LayoutParams lParams = new LinearLayout.LayoutParams(
                    matchParent, wrapContent);
            lParams.bottomMargin = 2;
            viewLayout.setBackgroundColor(Color.parseColor("#FFFFFF"));
            viewLayout.setGravity(17);*/

            ImageView ImgView =new ImageView(ctx);
            Picasso.with(ctx).load(pictureAuthor[position].toString()).into(ImgView);
            ImgView.setOnLongClickListener(new ClickListener());

            /*TextView textView=new TextView(viewLayout.getContext());
            textView.setText(String.valueOf(linkAuthor[position]));
            textView.setOnLongClickListener(new ClickListener());*/



            collection.addView(ImgView);

            return ImgView;
        }




        @Override
        public void destroyItem(ViewGroup collection, int position, Object view){
            collection.removeView((View) view);
        }

        @Override
        public int getCount(){
            return pages.size();
        }

        @Override
        public boolean isViewFromObject(View view, Object object){
            return view.equals(object);
        }

        @Override
        public int getItemPosition(Object object) {
            return POSITION_NONE;
        }

    }

public class ClickListener implements View.OnLongClickListener{

    @Override
    public boolean onLongClick(View v) {
        setPage(String.valueOf(linkAuthor[viewPager.getCurrentItem()]));
        Intent intent = new Intent(PagerViewActivity.this, WebPage.class);
        startActivity(intent);
        return false;
    }
}




}
