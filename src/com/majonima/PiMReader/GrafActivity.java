package com.majonima.PiMReader;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;

import static com.majonima.PiMReader.MainActivity.*;


/**
 * Created by Станислав on 20.04.2015.
 */
public class GrafActivity extends Activity implements View.OnClickListener {
    static LinearLayout swipe;
    ImageView ImgView;
    Object[] pictureGreen;
    Object[] pictureAutor;
    Object[] linkGreen;
    Object[] linkAutor;
    Bitmap Bm;
    int countGreen =0;
    int countAutor =-1;

    String selector="Autor";
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.grafical);
        ImgView =(ImageView) findViewById(R.id.image);
        //ImgView.setOnClickListener(GrafActivity.this);
        swipe=(LinearLayout) findViewById(R.id.swipe);
        pictureGreen = getPictureGreen().toArray();
        pictureAutor = getPictureAutor().toArray();
        linkGreen = getLinkGreen().toArray();
        linkAutor = getLinkAutor().toArray();
        getGraph(pictureGreen[countGreen].toString());

        ImgView.setOnTouchListener(new OnSwipeTouchListener(GrafActivity.this) {

            @Override
            public void onSwipeRight() {
                switch (selector) {
                    case "Green":
                        if (countGreen == 0) {
                            getGraph(pictureGreen[countGreen].toString());
                        } else {
                            getGraph(pictureGreen[--countGreen].toString());
                        }
                        break;
                    case "Autor":
                        if (countAutor<= 0) {
                            getGraph(pictureAutor[countAutor].toString());
                        } else {
                            getGraph(pictureAutor[--countAutor].toString());
                        }
                        break;
                }
            }
            @Override
            public void onSwipeLeft() {
                switch (selector) {
                    case "Green":

                            getGraph(pictureGreen[++countGreen].toString());

                        break;
                    case "Autor":

                            getGraph(pictureAutor[++countAutor].toString());

                        break;
                }

            }
            @Override
            public void onSwipeBottom() {
                selector="Autor";
                if(countAutor>=0) {
                    getGraph(pictureAutor[countAutor].toString());
                }else{
                    countAutor=0;
                    getGraph(pictureAutor[countAutor].toString());
                }
            }
            @Override
            public void onSwipeTop() {
                selector="Green";
                getGraph(pictureGreen[countGreen].toString());
            }
        });

    }
    public void getGraph(final String value){
        Thread th=new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    InputStream is=new URL(value).openStream();
                           Bm =BitmapFactory.decodeStream(is);
                    is.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });

        th.start();
        while(th.isAlive()){

        }
        setImgView(Bm);
    }
    public void setImgView(Bitmap bitmapFactory){
        ImgView.setImageBitmap(bitmapFactory);
    }



    @Override
    public void onClick(View v) {

        switch (selector) {
            case "Green":
                setPage(linkGreen[countGreen].toString());
                break;
            case "Autor":
                if(countAutor>=0) {
                    setPage(linkAutor[countAutor].toString());
                }else{
                    countAutor=0;
                    setPage(linkAutor[countAutor].toString());
                }

                break;
        }

        Intent intent = new Intent(GrafActivity.this, WebPage.class);
        startActivity(intent);

    }

    public class OnSwipeTouchListener implements View.OnTouchListener {

        private final GestureDetector gestureDetector;

        public OnSwipeTouchListener(Context context) {
            gestureDetector = new GestureDetector(context, new GestureListener());
        }

        public void onSwipeLeft() {
        }

        public void onSwipeRight() {
        }
        public void onSwipeBottom() {
        }

        public void onSwipeTop() {
        }

        public boolean onTouch(View v, MotionEvent event) {
            return gestureDetector.onTouchEvent(event);
        }

        private final class GestureListener extends GestureDetector.SimpleOnGestureListener {

            private static final int SWIPE_DISTANCE_THRESHOLD = 100;
            private static final int SWIPE_VELOCITY_THRESHOLD = 100;

            @Override
            public boolean onDown(MotionEvent e) {
                return true;
            }

            @Override
            public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
                float distanceX = e2.getX() - e1.getX();
                float distanceY = e2.getY() - e1.getY();
                if (Math.abs(distanceX) > Math.abs(distanceY) && Math.abs(distanceX) > SWIPE_DISTANCE_THRESHOLD && Math.abs(velocityX) > SWIPE_VELOCITY_THRESHOLD) {
                    if (distanceX > 0)
                        onSwipeRight();
                    else
                        onSwipeLeft();
                    return true;
                }else if (Math.abs(distanceY) > Math.abs(distanceX) && Math.abs(velocityY) > SWIPE_VELOCITY_THRESHOLD) {
                    if (distanceY> 0) {
                        onSwipeBottom();
                    } else {
                        onSwipeTop();
                    }
                }
                return false;
            }
        }
    }
}
