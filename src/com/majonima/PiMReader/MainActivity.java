package com.majonima.PiMReader;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.LinkedHashSet;

public class MainActivity extends Activity implements View.OnClickListener {

    static String page;
    static LinkedHashSet<String> article=new LinkedHashSet<String>();
    static LinkedHashSet<String> link=new LinkedHashSet<String>();
    static LinkedHashSet<String> linkAutor=new LinkedHashSet<String>();
    static LinkedHashSet<String> linkGreen=new LinkedHashSet<String>();
    static LinkedHashSet<String> pictureGreen =new LinkedHashSet<String>();
    static LinkedHashSet<String> pictureAutor =new LinkedHashSet<String>();
    static org.jsoup.nodes.Document doc = null;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        WebLinks("http://petrimazepa.com/page1");
        WebLinks("http://petrimazepa.com/page2");
        WebLinks("http://petrimazepa.com/page3");
        WebLinks("http://petrimazepa.com/page4");
//        WebLinks("http://petrimazepa.com/page5");
//        WebLinks("http://petrimazepa.com/page6");
//        WebLinks("http://petrimazepa.com/page7");
//        WebLinks("http://petrimazepa.com/page8");
//        WebLinks("http://petrimazepa.com/page9");

    }

    public static void WebLinks(final String weblink){
        Thread th=new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    doc = Jsoup.connect(weblink).get();
                    Elements urls = doc.select("a");
                    for (Element url : urls) {
                        if (url.toString().contains("Постоянная ссылка на ")) {
                            String text = url.toString();
                            article.add(text.substring(text.indexOf("Постоянная ссылка на ") + 21, text.indexOf("\" rel=\"bookmark\"")));
                            link.add(text.substring(text.indexOf("a href=\"") + 8, text.indexOf("\" title=")));


                            if(text.contains("\" class=\"attachment-thumbnail")&text.contains("247\"")) {
                                Log.d("URLka", text.substring(text.indexOf("7\" src=\"") + 8, text.indexOf("\" class=\"attachment-thumbnail")));
                                pictureGreen.add(text.substring(text.indexOf("247\" src=\"") + 10, text.indexOf("\" class=\"attachment-thumbnail")));
                                linkGreen.add(text.substring(text.indexOf("a href=\"") + 8, text.indexOf("\" title=")));

                            }
                            if(text.contains("\" class=\"attachment-thumbnail")&text.contains("747\"")) {
                                Log.d("URLka", text.substring(text.indexOf("7\" src=\"") + 8, text.indexOf("\" class=\"attachment-thumbnail")));
                                pictureAutor.add(text.substring(text.indexOf("747\" src=\"") + 10, text.indexOf("\" class=\"attachment-thumbnail")));
                                linkAutor.add(text.substring(text.indexOf("a href=\"") + 8, text.indexOf("\" title=")));
                                Log.d("array",String.valueOf(pictureAutor.size()));
                            }
                        }
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });
        th.start();
        while(th.isAlive()){
        }
    }
    public static LinkedHashSet<String> getLink() {
        return link;
    }

    public static LinkedHashSet<String> getArticle() {
        return article;
    }


    @Override
    public void onClick(View v) {
        /*Intent intent = new Intent(MyActivity.this, TextActivity.class);
        startActivity(intent);*/
        Intent intent;
        switch (v.getId()) {
            case R.id.ViewAsText:
                intent = new Intent(MainActivity.this, TextActivity.class);
        startActivity(intent);
                break;
            case R.id.ViewAsGraf:
                intent = new Intent(MainActivity.this, PagerViewActivity.class);
                startActivity(intent);
                break;
        }
    }
    public static String getPage() {
        return page;
    }

    public static void setPage(String page) {
        MainActivity.page = page;
    }

    public static LinkedHashSet<String> getPictureGreen() {
        return pictureGreen;
    }

    public static LinkedHashSet<String> getPictureAutor() {
        return pictureAutor;
    }
    public static LinkedHashSet<String> getLinkAutor() {
        return linkAutor;
    }

    public static LinkedHashSet<String> getLinkGreen() {
        return linkGreen;
    }
}
