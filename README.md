Экраны:
1.Основной (выбор графического или текстового просмотра списка статей)
Реализация: 2 кнопки для выбора способа просмотра. Получения через Jsoup списков: названий, ссылок, изображений. 
Добавить: автоматический запрос списков при окончании списка.
2. Текстовый (выбор статьи)
Реализация: автоматическая генерация списка статей на основе полученного в 1 пункте списка.
Добавить: автоматический запрос списков при окончании списка.
3.
  а) Графический (выбор статьи)
Реализация: PageViewer подгружающий картинки через списки пункта 1.
  б) Графический (выбор статьи) + выбор типа статей гринлайт\авторские или вертикальным свайпом перемещать между ними.
Реализация: Экран выбора + PageViewer подгружающий картинки через списки пункта 1.
4. Статья.
Реализация: WebViewer получающий адрес после выбора во 2 или 3 пункте. Возможно соединиться с сайтом для осуществления комментариев (Логин и пароль через настройки). Возможно при выборе пункта 2 реализация только текстовой составляющей без картинок\видео\комментариев.